import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule,FabContainer } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { DetailsPage } from '../pages/details/details';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AddpackagePage } from '../pages/addpackage/addpackage';
import { AddpaymentPage } from '../pages/addpayment/addpayment';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { MySplashPage } from '../pages/my-splash/my-splash';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DetailsPage,
    AddpaymentPage,
    AddpackagePage,
    MySplashPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    DetailsPage,
    AddpaymentPage,
    AddpackagePage,
    MySplashPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    InAppBrowser,
    SpinnerDialog,
    FabContainer,
    NativePageTransitions,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
