import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { LoginPage } from '../login/login';
import { DetailsPage } from '../details/details';
@IonicPage()
@Component({
  selector: 'page-my-splash',
  templateUrl: 'my-splash.html',
})
export class MySplashPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private app: App, public storage: Storage,private nativePageTransitions:NativePageTransitions ) {
    this.storage.get('fd_user').then((val)=>{
      console.log(val);
      if(val){
        setTimeout(() => {
          this.navCtrl.setRoot(HomePage,{data:val});       
         }, 2000); 
      }else{
        setTimeout(() => {
          this.navCtrl.setRoot(LoginPage);       
         }, 2000); 
      }
    })
         
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySplashPage');
  }
  ionViewWillLeave() {
    
     let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
       };
    
     this.nativePageTransitions.slide(options)
       .then(

       )
       .catch(
         
       );
    
    }

}
