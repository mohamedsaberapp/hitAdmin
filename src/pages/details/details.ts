import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, FabContainer } from 'ionic-angular';
import { AddpackagePage } from '../addpackage/addpackage';
import { AddpaymentPage } from '../addpayment/addpayment';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { leave } from '@angular/core/src/profile/wtf_impl';
import { LoginPage } from '../login/login';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  userToken: any;
  fd_token: any;
  myData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public http: Http, public storage: Storage, public fab: FabContainer, private toastCtrl: ToastController) {
    this.myData = this.navParams.get('data');
    this.userToken=this.myData.token;
    this.storage.get('fd_user').then((val) => {
      console.log(val);
      this.fd_token = val.token;
    })
  }
  //Method to override the default back button action
  scanNew() {
    let confirm = this.alertCtrl.create({
      title: 'Scan new athlete',
      message: 'Are you sure you want to scan another QR code ? ',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.navCtrl.setRoot(HomePage, { myT: this.fd_token });
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
  removePackage(p) {
    let confirm = this.alertCtrl.create({
      title: 'Remove package!',
      message: 'You are going to remove this package, Are you sure ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.http.get('http://hitapp.bit68.com/remove_package/?package_id=' + p.id + '&admin_id=' + this.fd_token + '&user_id='+this.userToken).map(res => res.json()).subscribe(data => {
              console.log(data);
              let toastDel = this.toastCtrl.create({
                message: 'Package removed successfully',
                duration: 1000,
                position: 'top'
              });
              toastDel.present();
              this.myData = data;
            }, err => {
              console.log(err);
            });
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }
  ionViewDidEnter(){
    this.myData = this.navParams.get('data');
    console.log('current data',this.myData);
  }
  addPackage() {
    this.navCtrl.push(AddpackagePage, { data: this.myData });
  }
  addPayment() {
    this.navCtrl.push(AddpaymentPage, { dataa: this.myData });
  }
  editInfo() {

    let prompt = this.alertCtrl.create({
      title: 'Edit Info',
      message: "Edit your personal information",
      inputs: [
        {
          name: 'Email',
          placeholder: 'Email'
        },
        {
          name: 'Address',
          placeholder: 'Address'
        },
        {
          name: 'Phone',
          placeholder: 'Phone number'
        },
        {
          name: 'Name',
          placeholder: 'Name'
        },
        {
          name: 'Height',
          placeholder: 'Height in CM'
        },
        {
          name: 'Weight',
          placeholder: 'Weight in KG'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          cssClass: 'danger',
          handler: data => {
            this.http.get('http://hitapp.bit68.com/update_info/?email=' + data.Email + '&name=' + data.Name + '&weight=' + data.Weight + '&height=' + data.Height + '&address=' + data.Address + '&phone_number=' + data.Phone + '&token=' + this.fd_token + '&QR=' + this.myData.qrcode).map(res => res.json()).subscribe(data => {
              console.log(data);
              this.myData = data;
            }, err => {
              console.log(err);
            });
          }
        }
      ]
    });
    prompt.present();
  }
  admit(p, c, fab: FabContainer) {
    // console.log('pack', p, '/', 'class', c);
    this.http.get('http://hitapp.bit68.com/admit/?token=' + this.fd_token + '&QR=' + this.myData.qrcode + '&package_id=' + p + '&class_id=' + c).map(res => res.json()).subscribe(data => {
      // console.log(fab);
      // fab.close();
      console.log(data);
      // this.myData = data;
      let toast = this.toastCtrl.create({
        message: 'Package admitted successfully',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        this.navCtrl.setRoot(DetailsPage, { data: data });
      });
    }, err => {
      console.log(err);
      alert(err);
    });
  }
  logOut(){
    this.storage.clear().then((val)=>{
      console.log(val);
      this.navCtrl.setRoot(LoginPage);
    })
  }
}
