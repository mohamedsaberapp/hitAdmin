import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScanResult } from '@ionic-native/barcode-scanner';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DetailsPage } from '../details/details';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myQr: string;
  myToken: any;
  fdInfo: any;
  myData: any;

  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private iab: InAppBrowser, public http: Http, public navParams: NavParams, private spinnerDialog: SpinnerDialog) {
    this.fdInfo = this.navParams.get('data');
    this.myData = {}
    if (this.navParams.get('data')) {
      this.myToken = this.fdInfo.token;
    } else {
      this.myToken = this.navParams.get('myT');
    }
  }
  ionViewDidEnter() {
    this.barcodeScanner.scan().then((barcodeData) => {
      // Success! Barcode data is here
      console.log(barcodeData);
      if(barcodeData.cancelled){
        this.navCtrl.setRoot(LoginPage);
      }else{
        this.myData = barcodeData;
        this.spinnerDialog.show('Loading information ...');
        this.http.get('http://hitapp.bit68.com/user_info_by_qr/?QR=' + barcodeData.text + '&token=' + this.myToken).map(res => res.json()).subscribe(data => {
          this.spinnerDialog.hide();
          console.log(data);
          this.navCtrl.setRoot(DetailsPage, { data: data });
        }, err => {
          console.log(err);
        });
      }
      
    }, (err) => {
      console.log(err);
    });
  }
}
