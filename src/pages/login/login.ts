import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { DetailsPage } from '../details/details';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  remember: boolean=false;
  loginForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,public http:Http,private formBuilder: FormBuilder,public storage:Storage) {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    
  }
  doLog(values){
    this.http.get('http://hitapp.bit68.com/login_front_desk/?email=' + values.email + '&password=' + values.password).map(res=>res.json()).subscribe(data=>{
      console.log(data);
      
      if(data.token){
        // if(this.remember){
        //   this.storage.set('fd_user',data);
        //   console.log('you will be remembered');
        // }
        this.storage.set('fd_user',data);       
        this.navCtrl.push(HomePage,{data:data});
      }else{
        alert(data);
      }
      
    },err=>{
      console.log(err);
    });
    
  }
}
