import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { DetailsPage } from '../details/details';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
/**
 * Generated class for the AddpaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addpayment',
  templateUrl: 'addpayment.html',
})
export class AddpaymentPage {
  payment: any={};
  payments: any;
  Qr: any;
  fd_token: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage,public http:Http,private spinnerDialog: SpinnerDialog,private toastCtrl:ToastController) {

    this.storage.get('fd_user').then((val)=>{
      this.fd_token=val.token;
    })
    this.Qr=this.navParams.get('dataa').qrcode;
    this.http.get('http://hitapp.bit68.com/all_payment_types/').map(res=>res.json()).subscribe(data=>{
      console.log(data);
      this.payments=data;
    },err=>{
      console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpaymentPage');
  }
  addPayment(values){
    console.log(values);
    this.spinnerDialog.show('Loading info ...','Please wait');
    this.http.get('http://hitapp.bit68.com/new_payment/?token=' + this.fd_token + '&QR=' + this.Qr + '&amount='+values.amount + '&title=' + values.title).map(res=>res.json()).subscribe(data=>{
      console.log(data);
      this.spinnerDialog.hide();
      let toast = this.toastCtrl.create({
        message: 'Payment added successfully',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        this.navCtrl.setRoot(DetailsPage,{data:data});
      });
      
    },err=>{
      console.log(err);
      this.spinnerDialog.hide();
    });
  }

}
