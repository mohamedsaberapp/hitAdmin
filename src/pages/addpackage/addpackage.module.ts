import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddpackagePage } from './addpackage';

@NgModule({
  declarations: [
    AddpackagePage,
  ],
  imports: [
    IonicPageModule,
  ],
})
export class AddpackagePageModule {}
