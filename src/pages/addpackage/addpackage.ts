import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { DetailsPage } from '../details/details';
/**
 * Generated class for the AddpackagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addpackage',
  templateUrl: 'addpackage.html',
})
export class AddpackagePage {
  nutrition: boolean;
  packageTypes: any;
  Qr: any;
  fd_token: any;
  myClasses: any = [];
  addPackForm: FormGroup;
  myBranches: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public formBuilder: FormBuilder, public storage: Storage,private toastCtrl:ToastController) {
    this.storage.get('fd_user').then((val) => {
      this.fd_token = val.token;
    })
    this.Qr = this.navParams.get('data').qrcode;
    this.addPackForm = this.formBuilder.group({
      branches: new FormControl('', Validators.required),
      expiry_date: new FormControl('', Validators.required),
      programs: new FormControl(''),
      packages: new FormControl(''),
      number_of_sessions: new FormControl('')
    });

    this.http.get('http://hitapp.bit68.com/all_branches/').map(res => res.json()).subscribe(data => {
      console.log(data);
      this.myBranches = data;
    }, err => {
      console.log(err);
    });
    this.http.get('http://hitapp.bit68.com/all_hit_classes/').map(res => res.json()).subscribe(data => {
      console.log(data);
      this.myClasses = data;
    }, err => {
      console.log(err);
    });
    this.http.get('http://hitapp.bit68.com/all_package_types/').map(res => res.json()).subscribe(data => {
      console.log(data);
      this.packageTypes = data;
    }, err => {
      console.log(err);
    });

  }
  chooseProg(e) {
    console.log(e);
    if (e == 12||e==20||e==9||e==10||e==14||e==21) {
      this.nutrition = true;
    } else {
      this.nutrition = false;
    }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpackagePage');
  }
  addPack(values) {
    console.log(values);
    this.http.get('http://hitapp.bit68.com/new_package/?token=' + this.fd_token + '&QR=' + this.Qr + '&expiry_date=' + values.expiry_date + '&branch_ids=' + values.branches + '&class_ids=' + values.programs + '&number_of_classes=' +values.number_of_sessions+ '&package_type_id=' + values.packages).map(res => res.json()).subscribe(data => {
      console.log('add pack',data);
      let toast = this.toastCtrl.create({
        message: 'Package added successfully',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
      toast.onDidDismiss(() => {
        this.addPackForm.reset();
        this.navCtrl.setRoot(DetailsPage,{data:data});
      });
    }, err => {
      console.log('add pack',err);
    });
  }

}
